FROM ubuntu:focal

RUN apt-get update
RUN apt-get -y upgrade

RUN DEBIAN_FRONTEND=noninteractive apt-get -y install python3 curl linux-libc-dev zlib1g-dev \
      git libdrm-dev pkg-config libwayland-dev libprotobuf-dev libgtest-dev cmake meson clang \
      rapidjson-dev libdocopt-dev libc6-arm64-cross libc6-dev-arm64-cross gcc-aarch64-linux-gnu \
      g++-aarch64-linux-gnu clang-format libweston-8-dev

# Fix libweston-8-protocols.pc
RUN echo "prefix=/usr \n\
includedir=\${prefix}/include \n\
datarootdir=\${prefix}/share \n\
pkgdatadir=\${pc_sysrootdir}\${datarootdir}/libweston-8/protocols \n\
Name: libWeston Protocols \n\
Description: libWeston protocol files \n\
Version: 8.0.0 \n\
Cflags: -I\${includedir}" > /usr/share/pkgconfig/libweston-8-protocols.pc

RUN ln -s /usr/bin/python3 /usr/bin/python

# IGT-GPU-Tools dependencies
RUN DEBIAN_FRONTEND=noninteractive apt-get -y install gcc flex bison libatomic1 libpciaccess-dev libkmod-dev libprocps-dev \
      libdw-dev liblzma-dev libcairo-dev libpixman-1-dev libudev-dev libxrandr-dev libxv-dev \
      x11proto-dri2-dev libunwind-dev libgsl-dev libasound2-dev libxmlrpc-core-c3-dev libjson-c-dev \
      libcurl4-openssl-dev valgrind peg autoconf automake xutils-dev libtool make

# IGT-GPU-Tools
RUN git clone https://gitlab.freedesktop.org/drm/igt-gpu-tools && \
    cd igt-gpu-tools && \
    meson build --libdir=lib --buildtype=release && ninja -C build install && \
    cd .. && rm -r igt-gpu-tools

RUN ldconfig

RUN apt-get clean
